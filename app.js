var CronJob = require('cron').CronJob;

// Fire Every 10 seconds
new CronJob('*/10 * * * * *', function(){
    log('Fired!');
}, null, true);

// Logs Message with Datestamp
function log(Message) {
	console.log('[' + new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString() + '] ' + Message)
}

